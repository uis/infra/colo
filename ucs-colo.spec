Name: ucs-colo
Version: 20151111
Release: 1
Summary: Web interface for the UIS Hosted Server Service
Group: System Environment/Daemons
Packager: UIS Platforms <unix-support@ucs.cam.ac.uk>
License: none
BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch
Source: ucs-colo-%{version}.tar.gz
Requires: ucs-mason-hs3-cs ucam-mason-bootstrap

%Description
This package contains the necessary code for the public Web interface
to University of Cambridge Information Services' Hosted Server Service.

%Prep
%setup -n %{name}-%{version}

%Install
mkdir -p -- "${RPM_BUILD_ROOT}/srv/www/colo"
cp * "${RPM_BUILD_ROOT}/srv/www/colo"
rm "${RPM_BUILD_ROOT}/srv/www/colo"/*.spec

%Files
%defattr(-,root,root)
/srv/www/colo

%Clean
rm -r "${RPM_BUILD_ROOT}"
